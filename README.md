# nexfil-docker
Dockerfile to build a container in which to run [nexfil](https://github.com/thewhiteh4t/nexfil).

## setup

```
git clone git@gitlab.com:opsminder/nexfil-docker.git
docker build -t nexfil .
```

## running
Make queries via:

```
docker run nexfil python3 nexfil.py -u xxx
```
