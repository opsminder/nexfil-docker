FROM python:3.8-alpine

RUN apk update
RUN apk add git

RUN git clone https://github.com/thewhiteh4t/nexfil.git /nexfil
WORKDIR /nexfil
RUN pip3 install -r requirements.txt

# Run via: docker run nexfil python3 nexfil.py -u xxx
CMD ["python3","nexfil.py"]